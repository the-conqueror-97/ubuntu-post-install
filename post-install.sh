#!/bin/sh


echo "Ubuntu debian post install";


apt-get update -y
apt-get upgrade -y

echo "Installing microsoft fonts"
add-apt-repository multiverse
apt update && sudo apt install -y ttf-mscorefonts-installer

fc-cache -f -v


echo "Installing SDK Man"
curl -s "https://get.sdkman.io" | bash
source "$HOME/.sdkman/bin/sdkman-init.sh"
sdk version